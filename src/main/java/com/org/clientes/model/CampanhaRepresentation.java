package com.org.clientes.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;

public class CampanhaRepresentation {

    private Long campanhaId;

    private String nomeCampanha;

    private Long timeId;


    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate dtVigenciaInicio;


    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate dtVigenciaFim;

    public CampanhaRepresentation(String nomeCampanha, Long timeId, LocalDate dtVigenciaInicio, LocalDate dtVigenciaFim) {
        this.nomeCampanha = nomeCampanha;
        this.timeId = timeId;
        this.dtVigenciaInicio = dtVigenciaInicio;
        this.dtVigenciaFim = dtVigenciaFim;
    }

    public Long getCampanhaId() {
        return campanhaId;
    }

    public CampanhaRepresentation() {
    }

    public void setCampanhaId(Long campanhaId) {
        this.campanhaId = campanhaId;
    }

    public String getNomeCampanha() {
        return nomeCampanha;
    }

    public void setNomeCampanha(String nomeCampanha) {
        this.nomeCampanha = nomeCampanha;
    }

    public Long getTimeId() {
        return timeId;
    }

    public void setTimeId(Long timeId) {
        this.timeId = timeId;
    }

    public LocalDate getDtVigenciaInicio() {
        return dtVigenciaInicio;
    }

    public void setDtVigenciaInicio(LocalDate dtVigenciaInicio) {
        this.dtVigenciaInicio = dtVigenciaInicio;
    }

    public LocalDate getDtVigenciaFim() {
        return dtVigenciaFim;
    }

    public void setDtVigenciaFim(LocalDate dtVigenciaFim) {
        this.dtVigenciaFim = dtVigenciaFim;
    }
}
