package com.org.clientes.restservice;

import com.org.clientes.business.ClientesService;
import com.org.clientes.exception.ClientesApiException;
import com.org.clientes.model.CampanhaRepresentation;
import com.org.clientes.model.Cliente;
import com.org.clientes.model.StatusRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/clientes")
public class ClientesApi {

    private ClientesService clientesService;
    private final static Logger log = LoggerFactory.getLogger(ClientesApi.class);

    @Autowired
    public ClientesApi(ClientesService clientesService) {
        this.clientesService = clientesService;
    }

    @GetMapping(value = "/listar", produces = "application/json;charset=UTF-8")
    public ResponseEntity listaClientes() {
        return new ResponseEntity(clientesService.listaClientes(), HttpStatus.OK);
    }

    @PutMapping(value = "/inserir", produces = "application/json;charset=UTF-8")
    public ResponseEntity inserirCliente(@RequestBody Cliente cliente) {
        try {

            return new ResponseEntity(clientesService.inserirCliente(cliente), HttpStatus.OK);
        } catch (ClientesApiException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity(convertResponse(false, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @JmsListener(destination = "queue.campanha")
    public void escutaFilaCampanha(CampanhaRepresentation campanha) {
        System.out.println(campanha.toString());
    }

    private StatusRepresentation convertResponse(boolean response, String mensagem) {

        StatusRepresentation statusRepresentation = new StatusRepresentation();
        statusRepresentation.setSistema("springboot-clientes");
        statusRepresentation.setStatus(mensagem);
        if (response) {
            statusRepresentation.setCodigo(0);
        } else {
            statusRepresentation.setCodigo(1);
        }
        return statusRepresentation;
    }
}
