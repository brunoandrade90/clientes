package com.org.clientes.repository;

import com.org.clientes.model.Cliente;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientesRepository extends CrudRepository<Cliente, Long> {

    List<Cliente> findByEmailEquals(String email);
}
