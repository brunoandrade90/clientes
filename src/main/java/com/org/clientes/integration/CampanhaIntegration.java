package com.org.clientes.integration;

import com.org.clientes.exception.ClientesApiException;
import com.org.clientes.model.CampanhaRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Optional;

@Repository
public class CampanhaIntegration {

    private RestTemplate restTemplate;

    private String baseUrlListaTime = "http://localhost:8080/campanha/listar/time";
    private String baseUrlAssociar = "http://localhost:8080/campanha/associar";


    public CampanhaIntegration() {
    }

    @Autowired
    public CampanhaIntegration(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public List<CampanhaRepresentation> listarCampanhasAtivas(Long id) throws ClientesApiException {
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(baseUrlListaTime)
                    .queryParam("id", id);

            ResponseEntity<List<CampanhaRepresentation>> lista = restTemplate.exchange(builder.build().toUri(), HttpMethod.GET, getHttpEntity(), new ParameterizedTypeReference<List<CampanhaRepresentation>>() {
            });
            return lista.getBody();
        } catch (Exception e) {
            throw new ClientesApiException("erro no servico de campanhas");
        }
    }

    public List<Optional<CampanhaRepresentation>> associarCampanhas(Long idCliente, Long idTime) throws ClientesApiException {
        try {

            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(baseUrlAssociar)
                    .queryParam("idCliente", idCliente).queryParam("idTime", idTime);
            ResponseEntity<List<Optional<CampanhaRepresentation>>> lista = restTemplate.exchange(builder.build().toUri(), HttpMethod.POST, getHttpEntity(), new ParameterizedTypeReference<List<Optional<CampanhaRepresentation>>>() {
            });
            return lista.getBody();
        } catch (Exception e) {
            throw new ClientesApiException("erro na chamada para Associar Campanhas, cliente inserido");
        }
    }

    private HttpEntity getHttpEntity() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity(httpHeaders);
    }
}
