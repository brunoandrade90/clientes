package com.org.clientes.business;

import com.org.clientes.exception.ClientesApiException;
import com.org.clientes.integration.CampanhaIntegration;
import com.org.clientes.model.CampanhaRepresentation;
import com.org.clientes.model.Cliente;
import com.org.clientes.repository.ClientesRepository;
import com.org.clientes.util.UtilCampos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClientesService {

    public ClientesService() {
    }


    private ClientesRepository clientesRepository;
    private CampanhaIntegration campanhaIntegration;

    @Autowired
    public ClientesService(ClientesRepository clientesRepository, CampanhaIntegration campanhaIntegration) {
        this.clientesRepository = clientesRepository;
        this.campanhaIntegration = campanhaIntegration;
    }

    public List<Cliente> listaClientes() {
        return (List<Cliente>) clientesRepository.findAll();
    }

    public List<CampanhaRepresentation> listaCampanhasAssociadas(Long timeId) throws ClientesApiException {
        return campanhaIntegration.listarCampanhasAtivas(timeId);
    }

    public List<Optional<CampanhaRepresentation>> inserirCliente(Cliente cliente) throws ClientesApiException {
        if (dadosClientesSaoValidos(cliente)) {
            List<Cliente> clientes = clientesRepository.findByEmailEquals(cliente.getEmail());
            if (clientes.size() == 0) {
                cliente = clientesRepository.save(cliente);
                return campanhaIntegration.associarCampanhas(cliente.getId(), cliente.getIdTimeCoracao());


            } else {
                return campanhaIntegration.associarCampanhas(clientes.get(0).getId(), cliente.getIdTimeCoracao());

            }
        }
        return null;

    }


    private boolean dadosClientesSaoValidos(Cliente cliente) throws ClientesApiException {
        StringBuffer mensagem = new StringBuffer();
        if (cliente != null) {
            if (UtilCampos.campoInvalido(cliente.getNome())) {
                mensagem.append("Nome ");
            }
            if (UtilCampos.emailInvalido(cliente.getEmail())) {
                mensagem.append(" E-mail");
            }
            if (UtilCampos.dataInvalida(cliente.getDataNascimento())) {
                mensagem.append(" Data Nascimento");
            }
            if (mensagem.toString().length() > 0) {
                mensagem.append(" invalidos");
                throw new ClientesApiException(mensagem.toString());
            }
            return true;
        } else {

            throw new ClientesApiException("Cliente vazio");
        }
    }

}
