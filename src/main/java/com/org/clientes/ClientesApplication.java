package com.org.clientes;

import com.org.clientes.model.Cliente;
import com.org.clientes.repository.ClientesRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@SpringBootApplication
public class ClientesApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClientesApplication.class, args);
    }



    @Bean
    CommandLineRunner runnerClientes(ClientesRepository repository) {
        DateTimeFormatter formatoBr = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        repository.save(new Cliente("Bruno Andrade", "bruno.andrade90@hotmail.com", LocalDate.parse("10/10/1990", formatoBr)));
        repository.save(new Cliente("Vinicius simão", "vini.simao90@hotmail.com", LocalDate.parse("11/10/1992", formatoBr)));
        repository.save(new Cliente("Emerson iversen Andrade", "Emerson.andrade90@hotmail.com", LocalDate.parse("10/10/1993", formatoBr)));
        repository.save(new Cliente("Cleiton silva", "cleiton.andrade90@hotmail.com", LocalDate.parse("10/11/1989", formatoBr)));
        return args -> repository.save(new Cliente("Julia Silva Andrade", "julia.andrade90@hotmail.com", LocalDate.parse("12/10/1990", formatoBr)));
    }

    @Bean
    public RestTemplate restTemplateCampanhas(RestTemplateBuilder builder) {
        return builder.build();
    }

}
