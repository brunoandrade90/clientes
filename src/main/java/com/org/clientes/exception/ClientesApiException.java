package com.org.clientes.exception;

public class ClientesApiException extends Exception {

    public ClientesApiException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public ClientesApiException(String message) {
        super(message);
    }
}
