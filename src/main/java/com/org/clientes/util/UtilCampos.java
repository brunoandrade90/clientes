package com.org.clientes.util;

import org.apache.tomcat.jni.Local;

import java.time.LocalDate;
import java.util.regex.Pattern;

public class UtilCampos {

    /**
     * Retorna true para campo Invalido
     *
     * @param campo
     * @return
     */
    public static boolean campoInvalido(String campo) {
        if (campo == null || campo.equals(" ")) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean dataInvalida(LocalDate dataNascimento) {

        if (dataNascimento == null) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean emailInvalido(String email) {
        if (email == null)
            return true;

        String regex = "^(.+)@(.+)$";

        Pattern pattern = Pattern.compile(regex);
        return !pattern.matcher(email).matches();
    }

}
