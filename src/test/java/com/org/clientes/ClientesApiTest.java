package com.org.clientes;


import com.org.clientes.model.Cliente;
import com.org.clientes.restservice.ClientesApi;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
public class ClientesApiTest {

    @Autowired
    private ClientesApi clientesApi;

    @Test
    public void testeInserirCliente() {
        ResponseEntity responseEntity = clientesApi.inserirCliente(getCliente());
        assertEquals(HttpStatus.OK.value(), responseEntity.getStatusCode().value());
    }


    @Test
    public void testeListarCliente() {
        ResponseEntity responseEntity = clientesApi.listaClientes();
        assertEquals(HttpStatus.OK.value(), responseEntity.getStatusCodeValue());
    }

    private Cliente getCliente() {
        Cliente cliente = new Cliente();
        cliente.setNome("bbbb b");
        cliente.setDataNascimento(LocalDate.parse("1990-10-10"));
        cliente.setEmail("bruno.andrade90@hotmail.com");
        cliente.setIdTimeCoracao(1L);
        return cliente;
    }


}
