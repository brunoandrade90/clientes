package com.org.clientes;


import com.org.clientes.business.ClientesService;
import com.org.clientes.exception.ClientesApiException;
import com.org.clientes.integration.CampanhaIntegration;
import com.org.clientes.model.CampanhaRepresentation;
import com.org.clientes.model.Cliente;
import com.org.clientes.repository.ClientesRepository;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@SpringBootTest
public class ClientesServiceTest {

    private CampanhaIntegration campanhaIntegration;

    private ClientesRepository clientesRepository;

    private ClientesService clientesService;

    List<CampanhaRepresentation> campanhaRepresentationList;

    @BeforeEach
    public void setup() {
        campanhaIntegration = Mockito.mock(CampanhaIntegration.class);
        clientesService = Mockito.mock(ClientesService.class);
        clientesRepository = Mockito.mock(ClientesRepository.class);
    }

    @Test
    public void clienteServiceCampanhas() throws ClientesApiException {
        Mockito.when(campanhaIntegration.listarCampanhasAtivas(1L)).thenReturn(new ArrayList<>());
        List<CampanhaRepresentation> campanhaRepresentations = clientesService.listaCampanhasAssociadas(1L);
        Assert.assertNotNull(campanhaRepresentations);
    }

    @Test
    public void inserirCliente() throws ClientesApiException {
        List<Cliente> clientes = new ArrayList<>();
        clientes.add(getCliente());
        Mockito.when(campanhaIntegration.listarCampanhasAtivas(1L)).thenReturn(new ArrayList<>());
        Mockito.when(clientesRepository.findByEmailEquals("s")).thenReturn(clientes);
        clientesService.inserirCliente(getCliente());

    }


    private Cliente getCliente() {
        Cliente cliente = new Cliente();
        cliente.setNome("bbbb b");
        cliente.setDataNascimento(LocalDate.parse("1990-10-10"));
        cliente.setEmail("bruno.andrade90@hotmail.com");
        cliente.setIdTimeCoracao(1L);
        return cliente;
    }

}
